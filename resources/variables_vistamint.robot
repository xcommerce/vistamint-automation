*** Variables ***
${BROWSER}               ${ROBOT_BROWSER}

&{SERVER}               live=https://www.vistamint.eu/en/

${VALID_USER}          alexandra@xcommerce.eu
${VALID_PASSWORD}      123456

${LOGIN_URL}            ${SERVER.${ENVIRONMENT}}login?back=my-account

${HOMEPAGE_URL}         ${SERVER.${ENVIRONMENT}}

${ACCOUNT_ORDERS}       ${SERVER.${ENVIRONMENT}}identity

${PAYPLAZA_PAGE}        https://pay.mollie.nl/payment/start/c2f09fda09089f9a73e62ac1c38fbe79

${CART_PAGE}            ${SERVER.${ENVIRONMENT}}cart?action=show

${ORDER_PAGE}           ${SERVER.${ENVIRONMENT}}order

${ACCESSORIES_PAGE}     ${SERVER.${ENVIRONMENT}}384-accessories

${CUSTOMIZE_PAGE}       ${SERVER.${ENVIRONMENT}}module/coincustomization/customcoin?id_product=29260

#  USER _________________________________________________________
${DELAY}                0

# Submit Button From Cart
${submit_button_from_cart}    css=div[class="text-xs-left"]

# Submit address button
${submit_address}             css=span[class="custom-radio"]

# Submit payment button
${submit_payment}             css=button[class="button button--primary large full"]
${checkbox_payment}           xpath=//*[@id="payment-option-1-container"]/div/span

# Payment details
${card_holder}                Alexandra Xcommerce
${visa_card_number}           4111111111111111
${card_expires_date}          1234
${card_cvv}                   123
${card_cvv_field}             css=input[name="card_cvv"]

#Details Page
${view_cart}                  css=a[class="button button--primary"]
${button_add_to_cart1}        css=#js-product-list > div > article:nth-child(4) > div > div.small-right > div.product-list-add-block.clearfix > form > a
${button_add_to_cart2}        css=button[data-button-action="add-to-cart"]
${button_add_to_cart3}        css=button[class="btn btn_addcrt"]
${first_product_add}          css=div[class="cartblock-item__info"]

${get_price0}                 €0.00

# Header
${login_button}               css=a[class="button button--header-main"]
${search_field}               css=input[class="form-control ui-autocomplete-input"]
${search_button}              css=div[id="_desktop_search"]

#Customize Page
${add_text_face2}              xpath=//div[@id="cright_side"]/div[3]/div[2]/div/div[4]/h5/a/span[2]/i
${choose_picture_library_face1}     xpath=//div[@id="cright_side"]/div[3]/div[1]/div/div[2]/div/div[2]/div[2]
${legs_picture_library_face1}   css=img[data-standard-die="VMMT105097"]



