*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library             Selenium2Library

*** Keywords ***

Open Browser To Homepage
    Open Browser                        ${HOMEPAGE_URL}   ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed                  ${DELAY}

    Wait Until Element Is Visible       css=section[id="content"]   timeout=15


Open Login Page

    ${do_not_visible_login}              ${value}=       run keyword and ignore error   Element Should Not Be Visible       css=div[class="user-info dropdown"]
    Run Keyword If	                    '${do_not_visible_login}' == 'PASS'    Reload Page

    Wait Until Element Is Visible       css=div[class="user-info dropdown"]   timeout=15

    Wait Until Element Is Visible       ${login_button}   timeout=15
    Click Element                       ${login_button}

    Wait Until Element Is Visible       css=section[class="page-content clearfix"]   timeout=15

    Location Should Be                  ${LOGIN_URL}

    Close Cookie

Complete With Valid Dates And Submit User Login
    Insert Valid Email
    Insert Valid Password
    Submit User Login And Check Location Page

    Go To Home Page

Insert Valid Email
    Wait Until Element Is Visible       css=input[name="email"]
    Input Text                          email           ${VALID_USER}

Insert Valid Password
    Wait Until Element Is Visible       css=input[name="password"]
    Input Text                          password          ${VALID_PASSWORD}

Submit User Login And Check Location Page
    Click Button                        css=button[class="button button--primary"]
    Wait Until Element Is Visible       css=div[class="col-xs-12 col-lg-3"]    timeout=15

    Location Should Be                  ${ACCOUNT_ORDERS}

Go To Home Page
    Click Element                       css=div[id="_desktop_logo"]
    Location Should Be                  ${HOMEPAGE_URL}

Scroll Page To Location
    [Arguments]                          ${x_location}    ${y_location}
    Execute JavaScript                   window.scroll(${x_location},${y_location})

Close Cookie
    Wait Until Element Is Visible         css=div[id="gdpr_cookie_popup"]    timeout=15
    Click Element                         css=a[class="accept_gdrp button button--primary add-to-cart"]

    Wait Until Element Is Not Visible     css=div[id="gdpr_cookie_popup"]     timeout=15

#Close Popup
#    Wait Until Element Is Visible         css=div[id="rm_row"]    timeout=15
#    Click Element                         css=div[class="bottomtext"]
#
#    Wait Until Element Is Not Visible     css=div[id="rm_row"]     timeout=15

Condition For Popup
    ${popup_close}                         ${value}=       run keyword and ignore error   Element Should Not Be Visible       css=div[id="rm_row"]
    Run Keyword If	                       '${do_not_visible_login}' == 'PASS'    Close Popup

Add To Cart Button Selection (For The First Product Added)
    Wait Until Element Is Visible        ${button_add_to_cart1}        timeout=15
    Click Element                        ${button_add_to_cart1}

    Product Should Be To Successfully Add To Cart

Add To Cart Button Selection (For The Second Product Added)
    Wait Until Element Is Visible        ${button_add_to_cart2}        timeout=15
    Click Element                        ${button_add_to_cart2}

    Product Should Be To Successfully Add To Cart

Add To Cart Button Selection (For The Last Product Added)
    Wait Until Element Is Visible        ${button_add_to_cart3}        timeout=15
    Click Element                        ${button_add_to_cart3}

    Wait Until Element Is Visible        css=div[class="modal-title text-xs-center"]       timeout=15

Cart Button Should Be Visible
    ${cart_button_visible}              ${value}=       run keyword and ignore error   Wait Until Element Is Visible       ${cart_button}        timeout=15
    Run Keyword If	                    '${cart_button_visible}' == 'PASS'    Reload Page

Open the Accessories page
    Wait Until Element Is Visible        css=div[class="hidden-md-down pull-lg-right"] > :nth-child(4)        timeout=15

    Click Element                        css=div[class="hidden-md-down pull-lg-right"] > :nth-child(4)
    Wait Until Element Is Visible        css=div[id="content-wrapper"]        timeout=15

    Location Should Be                   ${ACCESSORIES_PAGE}

Add To Cart A Product From Accessories Page
    Wait Until Element Is Visible        css=#js-product-list > div > article:nth-child(4)     timeout=15

    Add To Cart Button Selection (For The First Product Added)

Product Should Be To Successfully Add To Cart
    Wait Until Element Is Visible        css=div[class="cart-summary cart-summary--b2c"]     timeout=15

    Location Should Be                   ${CART_PAGE}

Open The Popup For The Last Product
    Go Back

    Wait Until Element Is Visible        css=#js-product-list > div > article:nth-child(8)     timeout=15
    Click Element                        css=#js-product-list > div > article:nth-child(8)

    Wait Until Element Is Visible        css=div[class="modal-content"]     timeout=15

Open The Customize Page
    Wait Until Element Is Visible        css=div[class="hidden-md-down pull-lg-right"] > :nth-child(6)        timeout=15
    Click Element                        css=div[class="hidden-md-down pull-lg-right"] > :nth-child(6)

    Wait Until Element Is Visible        css=div[class="cright_side_in clearfix"]        timeout=15

    Location Should Be                   ${CUSTOMIZE_PAGE}

Customize The First Token's Face (Picture)
    Wait Until Element Is Visible        ${choose_picture_library_face1}        timeout=15
    Click Element                        ${choose_picture_library_face1}

    Wait Until Element Is Visible        css=div#choose_library > div > div        timeout=15

    Choose a picture site library

    sleep                                1s
    Element Should Not Be Visible        css=div#choose_library > div > div

Choose a picture site library
    Element Should Be Visible            ${legs_picture_library_face1}
    Click Image                          ${legs_picture_library_face1}

    Wait Until Element Is Visible        css=button[class="btn btn-default"]        timeout=15
    Click Element                        css=button[class="btn btn-default"]

Customize The Another Token's Face (Text)
    Wait Until Element Is Visible        ${add_text_face2}        timeout=15
    Click Element                        ${add_text_face2}

    Add Text                             Xcommerce Test

Add Text
    Wait Until Element Is Visible        xpath=//div[@id="cright_side"]/div[3]/div[2]/div/div[4]/div/div[1]/div/div[1]/input       timeout=15

    [Arguments]                          ${insert_text}
    Input Text                           xpath=//div[@id="cright_side"]/div[3]/div[2]/div/div[4]/div/div[1]/div/div[1]/input    ${insert_text}

Go To Cart
    Wait Until Element Is Visible        ${view_cart}        timeout=15
    Click Element                        ${view_cart}

    Location Should Be                   ${CART_PAGE}

Submit Button From Cart
    Wait Until Element Is Visible        ${submit_button_from_cart}   timeout=15
    Click Element                        ${submit_button_from_cart}

    Wait Until Element Is Visible        css=div[class="js-address-form"]    timeout=15

    Location Should Be                  ${ORDER_PAGE}

Submit Current Addresses
    Wait Until Element Is Visible       ${submit_address}   timeout=15
    Click Element                       ${submit_address}

    Wait Until Element Is Visible       css=div[class="payment-options MB30"]      timeout=15

Check The Price For Products
    ${get_price1}=                      Get Text    xpath=//*[@id="main"]/div/div[1]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/strong
    ${get_price2}=                      Get Text    xpath=//*[@id="main"]/div/div[1]/div[2]/div/div[2]/div[2]/div/div[2]/div[2]/strong
    ${get_price3}=                      Get Text    xpath=//*[@id="main"]/div/div[1]/div[2]/div/div[3]/div[2]/div/div[2]/div[2]/strong

# Product price is compared with zero value:
     Should Not Be Equal                  ${get_price1}   ${get_price0}
     Should Not Be Equal                  ${get_price2}   ${get_price0}
     Should Not Be Equal                  ${get_price3}   ${get_price0}

Select Payment and submit
    sleep                               2s
    scroll page to location             0   1000

    Wait Until Element Is Visible       ${checkbox_payment}         timeout=15
    Click Element                       ${checkbox_payment}

    Click Element                       css=span[class="custom-checkbox"]

    Wait Until Element Is Visible       ${submit_payment}           timeout=15
    Click Button                        ${submit_payment}

Complete card holder
    [Arguments]                         ${card_holder}
    Input Text                          css=input[name="cc-name"]    ${card_holder}

Complete card number
    Wait Until Element Is Visible       css=input[id="cc-number"]    timeout=15
    [Arguments]                         ${card_number}
    Input Text                          css=input[id="cc-number"]    ${card_number}
    Unselect Frame

Complete card expires date
    [Arguments]                         ${card_holder}
    Input Text                          css=input[name="cc-exp"]     ${card_expires_date}

Complete card cvv
    Click Element                       ${card_cvv_field}
    [Arguments]                         ${card_cvv}
    Input Text                          ${card_cvv_field}             ${card_cvv}

Complete all card details in payment page
    Complete card holder                ${card_holder}
    Complete card number                ${visa_card_number}
    Complete card expires date          ${card_expires_date}
    Complete card cvv                   ${card_cvv}






