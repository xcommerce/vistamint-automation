*** Settings ***
Suite Setup       Open Browser To Homepage
#Suite Teardown    Close All Browsers

Resource         ../resources/resource_vistamint.robot
Resource         ../resources/variables_vistamint.robot

*** Test Cases ***
Customer should be able to successfully login
    Open Login Page
    Complete With Valid Dates And Submit User Login

Customer should be able to add the second product to cart from the Accessories page
    Open the Accessories page
    Add To Cart A Product From Accessories Page

Customer should be able to add the last product to cart from the popup
    Open The Popup For The Last Product
    Add To Cart Button Selection (For The Second Product Added)

Customer should be able to customize a token and add to cart
    Open The Customize Page
    Customize The First Token's Face (Picture)
    Customize The Another Token's Face (Text)
    Add To Cart Button Selection (For The Last Product Added)

Customer should be able to go the cart page and check the products prices
    Go To Cart
    Check The Price For Products
    Submit Button From Cart

Customer should be able to select an address, Payment and submit step
    Submit Current Addresses
    Select Payment and submit

Customer should be able to complete the card details
    Complete all card details in payment page



#   robot -d results -v ENVIRONMENT:live -v BROWSER:chrome tests\01_order.robot